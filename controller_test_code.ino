
// array of inputs on the Arduino Pro Micro
int inputPins[] = {A0, A1, A2, A3, 4, 5, 6, 7, 8, 9, 10, 16, 14, 15};

void setup()
{    
    Serial.begin(115200);

    // count through each of the 14 inputs
    for (int i = 0; i < 14; i++)
    {
        if (i < 4) // initialize the 4 analog inputs
        {
            pinMode(inputPins[i], INPUT);
        }
        else // then the rest of the digital inputs
        {
            pinMode(inputPins[i], INPUT_PULLUP);
        }
    }
}

void loop()
{
    // loop thorugh each input and print their values in one line
    for (int i = 0; i < 14; i++)
    {
        Serial.print(i);
        Serial.print(": ");
        
        if (i < 4) // ANALOG LOGIC
        {
            int val = analogRead(inputPins[i]);
            Serial.print(val);

            
        }
        else // DIGITAL LOGIC
        {
            bool val = digitalRead(inputPins[i]);
            Serial.print(val);
        }
        Serial.print("\t");
    }
    Serial.println();

    delay(10);
}